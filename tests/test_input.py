import os.path

import pytest

from .context import pyatran

import pyatran.input
from pyatran.params import load_json as load_sciaconf


def mk_target_dir(tmpdir, name):
    target = os.path.join(str(tmpdir.mkdir(name)), 'DATA_IN')
    os.makedirs(target)
    return target


def assert_path(actual, required):
    assert os.path.normpath(actual) == os.path.normpath(required)


# TODO remove tmpdir after test finishes

class TestRelativePaths(object):

    home = os.path.expanduser('~')

    @pytest.fixture(autouse=True)
    def setup_class(self, tmpdir):
        self.data_dir = os.path.join(os.path.dirname(__file__), 'data')
        self.input_dir = os.path.join(self.data_dir, 'input')
        sciaconf = load_sciaconf(os.path.join(self.data_dir,
                                              'sciaconf_inputfiles.json'))
        sciaconf['base_directory'] = self.input_dir
        # TODO for now, we don't test everything.
        for k in sciaconf.keys():
            if k not in ['base_directory', 'control.inp', 'control_prof.inp',
                         'control_ac.inp', 'control_aer.inp', 'control_ray.inp',
                         'man_aer.inp', 'control_rrs.inp', 'xsections.inp']:
                all_this_keys = [kk for kk in sciaconf[k].keys()]
                for kk in all_this_keys:
                    del sciaconf[k][kk]
        self.sciaconf = sciaconf

    def test_controlinp(self, tmpdir):
        target = mk_target_dir(tmpdir, 'cinp0')
        cfg = pyatran.input.input._save_input_files_controlinp(
            self.sciaconf, target, self.input_dir, self.home)
        d = 'DATA_IN'
        assert_path(cfg['Include land fluorescence'][1], os.path.join(d, 'cinp_fluor.dat'))
        assert_path(cfg['Filename user provided solar spectrum'], os.path.join(d, 'cinp_solspec.dat'))
        assert_path(cfg['Wavelength grid from file'][1], os.path.join(d, 'cinp_wlaxis.dat'))
        assert_path(cfg['Path to CIA data base'], d)
        assert_path(cfg['Spectral albedo filename'], 'spectral_albedo.dat')
        assert_path(cfg['Path to the albedo database'], d)
        # TODO test for "Replacement for water"
        assert_path(cfg['Standard profile scenario file name'], os.path.join(d, 'cinp_prof_std.dat'))
        assert_path(cfg['Standard profile scenario for line absorbers'], os.path.join(d, 'cinp_prof_line_std.dat'))
        assert_path(cfg['Path to climatology data base'], d)

    def test_controlac(self, tmpdir):
        target = mk_target_dir(tmpdir, 'cac0')
        cfg = pyatran.input.input._save_input_files_controlacinp(
            self.sciaconf, target, self.input_dir, self.home)
        d = 'DATA_IN'
        assert cfg['Integration nodes'][1] == os.path.join(d, 'cac_intnodes.txt')

    def test_controlaer(self, tmpdir):
        target = mk_target_dir(tmpdir, 'caer0')
        cfg = pyatran.input.input._save_input_files_controlaerinp(
            self.sciaconf, target, self.input_dir, self.home)
        d = 'DATA_IN'
        assert cfg['Path to Aerosol data base'] == os.path.join(d, '')

    @pytest.mark.skip(reason='test not implemented yet')
    def test_controlbrdf(self, tmpdir):
        raise NotImplementedError

    @pytest.mark.skip(reason='test not implemented yet')
    def test_controlout(self, tmpdir):
        raise NotImplementedError

    def test_controlprof(self, tmpdir):
        target = mk_target_dir(tmpdir, 'cprf0')
        cfg = pyatran.input.input._save_input_files_controlprofinp(
            self.sciaconf, target, self.input_dir, self.home)
        d = 'DATA_IN'
        assert_path(cfg['Pressure and temperature file name'][0], os.path.join(d, 'cprof_ptprof.dat'))
        assert_path(cfg['Use CIRA model'][1], d)
        assert_path(cfg['Trace gas replacement profiles'][0][0], os.path.join(d, 'cprof_o3prof.dat'))
        assert_path(cfg['Trace gas replacement profiles'][1][0], os.path.join(d, 'cprof_no2prof.dat'))
        assert_path(cfg['BrO climatology file'], os.path.join(d, 'cprof_broclim.dat'))

    def test_controlray(self, tmpdir):
        target = mk_target_dir(tmpdir, 'cray0')
        cfg = pyatran.input.input._save_input_files_controlrayinp(
            self.sciaconf, target, self.input_dir, self.home)
        d = 'DATA_IN'
        assert cfg['Rayleigh depolarisation filename'] == os.path.join(d, 'cray_king.dat')

    @pytest.mark.skip(reason='test not implemented yet')
    def test_controlret(self, tmpdir):
        raise NotImplementedError

    def test_controlrrs(self, tmpdir):
        target = mk_target_dir(tmpdir, 'crr0')
        cfg = pyatran.input.input._save_input_files_controlrrsinp(
            self.sciaconf, target, self.input_dir, self.home)
        d = 'DATA_IN'
        assert_path(cfg['Use spin-rotational splitting'][1], os.path.join(d, 'crrs_o2et.dat'))
        assert_path(cfg['Use spin-rotational splitting'][2], os.path.join(d, 'crrs_o2pt.dat'))

    @pytest.mark.skip(reason='test not implemented yet')
    def test_controlwf(self, tmpdir):
        raise NotImplementedError

    @pytest.mark.skip(reason='test not implemented yet')
    def test_lowaer(self, tmpdir):
        raise NotImplementedError

    def test_manaer(self, tmpdir):
        target = mk_target_dir(tmpdir, 'maer0')
        cfg = pyatran.input.input._save_input_files_manaerinp(
            self.sciaconf, target, self.input_dir, self.home)
        d = 'DATA_IN'
        assert_path(cfg['Source for aerosol extinction coefficients'][0], os.path.join(d, 'maer_extc1.dat'))
        assert_path(cfg['Source for aerosol extinction coefficients'][1], os.path.join(d, 'maer_extc2.dat'))
        assert_path(cfg['Directory name for scattering matrices'], d)
        # TODO test "File names containing expansion coefficients"
        # TODO test "File names containing scattering function/matrix"

    @pytest.mark.skip(reason='test not implemented yet')
    def test_wmoaer(self, tmpdir):
        raise NotImplementedError

    @pytest.mark.skip(reason='test not implemented yet')
    def test_wmogen(self, tmpdir):
        raise NotImplementedError

    def test_xsections(self, tmpdir):
        target = mk_target_dir(tmpdir, 'xs0')
        cfg = pyatran.input.input._save_input_files_xsectionsinp(
            self.sciaconf, target, self.input_dir, self.home)
        d = 'DATA_IN'
        assert cfg['X-section: o3 UV-NIR'][0][0] == 'xs_o3.dat'
        assert_path(cfg['X-section path'], d)

    @pytest.mark.skip(reason='not all control files tested yet')
    def test_all(self, tmpdir):
        runcfg = pyatran.input.save_input_files(
            self.sciaconf, self.runtime_dir)
        assert runcfg == 1

    @pytest.mark.skip()
    def test_controlinp_cia_notimplemented(self, tmpdir):
        # "Collision-induced absorption" != "NONE" raises NotImplementedError
        raise NotImplementedError

    @pytest.mark.skip()
    def test_controlprof_ozoneclim_notimplemented(self, tmpdir):
        # "Ozone climatology" !="NONE" raises NotImplementedError
        raise NotImplementedError
        assert cfg['Path to ozone climatology'] == os.path.join(d, 'o3clim', '')  # noqa


def test_mk_dirpath():
    required = os.path.join('DATA_IN', 'blabla', '')
    assert pyatran.input.input.mk_dirpath('blabla') == required
